<?php

namespace App\Http\Requests\Station;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'max:255'],
            'company_id' => ['nullable', 'integer', 'exists:companies,id']
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [];
    }
}
