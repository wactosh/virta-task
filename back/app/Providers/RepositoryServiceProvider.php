<?php

namespace App\Providers;

use App\Repositories\BaseRepository;
use App\Repositories\Company\CompanyRepository;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Station\StationRepository;
use App\Repositories\Station\StationRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @inheritDoc
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(CompanyRepositoryInterface::class, CompanyRepository::class);
        $this->app->bind(StationRepositoryInterface::class, StationRepository::class);
    }
}
