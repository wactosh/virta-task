<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\CreateRequest;
use App\Http\Requests\Company\UpdateRequest;
use App\Services\CompanyService;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * CompanyController constructor.
     *
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return success('Companies list', $this->companyService->getAllWithChildCompanies());
    }

    /**
     * @param CreateRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $inputs = $request->only(['name', 'parent_company_id']);

        $result = $this->companyService->createOne($inputs);

        if ($result) {
            return success('Company created', $result, 201);
        }

        return failed('An error occurred while creating company', 500);
    }

    /**
     * @param int $companyId
     *
     * @return JsonResponse
     */
    public function show(int $companyId)
    {
        return success('Company detail', $this->companyService->findWithChildCompanies($companyId));
    }

    /**
     * @param UpdateRequest $request
     * @param int           $companyId
     *
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, int $companyId)
    {
        $inputs = $request->only(['name', 'parent_company_id']);

        $result = $this->companyService->updateOne($companyId, $inputs);

        if ($result) {
            $updatedCompany = $this->companyService->findWithChildCompanies($companyId);

            return success('Company updated', $updatedCompany);
        }

        return failed('An error occurred while updating company', 500);

    }

    /**
     * @param int $companyId
     *
     * @return JsonResponse
     */
    public function destroy(int $companyId)
    {
        $result = $this->companyService->deleteOne($companyId);

        if ($result) {
            return success('Company deleted');
        }

        return failed('An error occurred while deleting company', 500);
    }

    /**
     * @param int $companyId
     *
     * @return JsonResponse
     */
    public function stations(int $companyId)
    {
        return success('Company children and stations', $this->companyService->findStationsWithChildCompanies($companyId));
    }
}
