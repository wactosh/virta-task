<?php

/**
 * @OA\Post(
 *      path="/stations/",
 *      summary="Create station",
 *      description="Create a station",
 *      operationId="createStation",
 *      tags={"Station"},
 *
 *      @OA\RequestBody(
 *          required=true,
 *          description="Create station request",
 *          @OA\JsonContent(
 *              required={"name", "latitude", "longitude", "company_id"},
 *              @OA\Property(property="name", type="string", example="station name"),
 *              @OA\Property(property="latitude", type="number", example=1.1111),
 *              @OA\Property(property="longitude", type="number", example=1.1111),
 *              @OA\Property(property="company_id", type="number", example=1),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Station created"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<station>",
 *              )
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Invalid request response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=false),
 *              @OA\Property(property="message", type="string", example="The given data was invalid."),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="name",
 *                      type="string",
 *                      example={
 *                          "The name field is required.",
 *                      },
 *                  ),
 *                  @OA\Property(
 *                      property="latitude",
 *                      type="string",
 *                      example={
 *                          "The latitude field is required.",
 *                      },
 *                  ),
 *                  @OA\Property(
 *                      property="longitude",
 *                      type="string",
 *                      example={
 *                          "The longitude field is required.",
 *                      },
 *                  ),
 *                  @OA\Property(
 *                      property="company_id",
 *                      type="string",
 *                      example={
 *                          "The company id field is required.",
 *                      },
 *                  ),
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Get(
 *      path="/stations/",
 *      summary="Get stations",
 *      description="Get all stations or stations within radius of 1KM if latitude & longitude are provided",
 *      operationId="getStations",
 *      tags={"Station"},
 *
 *      @OA\Parameter(
 *          parameter="latitude",
 *          name="latitude",
 *          in="query",
 *          required=false,
 *          description="Station latitude",
 *          @OA\Schema(
 *              type="number"
 *          )
 *     ),
 *
 *      @OA\Parameter(
 *          parameter="longitude",
 *          name="longitude",
 *          in="query",
 *          required=false,
 *          description="Station longitude",
 *          @OA\Schema(
 *              type="number"
 *          )
 *     ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Stations list"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<stations>",
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Get(
 *      path="/stations/{stationId}",
 *      summary="Get station",
 *      description="Get a station including sub stations",
 *      operationId="getStation",
 *      tags={"Station"},
 *
 *      @OA\Parameter(
 *          description="ID of station",
 *          in="path",
 *          name="stationId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Station detail"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<station>",
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Put(
 *      path="/stations/{stationId}",
 *      summary="Update station",
 *      description="Update a station",
 *      operationId="updateStation",
 *      tags={"Station"},
 *
 *      @OA\Parameter(
 *          description="ID of station",
 *          in="path",
 *          name="stationId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\RequestBody(
 *          description="Create station request",
 *          @OA\JsonContent(
 *              @OA\Property(property="name", type="string", example="station name"),
 *              @OA\Property(property="latitude", type="number", example=1.1111),
 *              @OA\Property(property="longitude", type="number", example=1.1111),
 *              @OA\Property(property="company_id", type="number", example=1),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Station updated"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<station>",
 *              )
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Invalid request response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=false),
 *              @OA\Property(property="message", type="string", example="The given data was invalid."),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="parent_station_id",
 *                      type="string",
 *                      example={
 *                          "The selected company id is invalid.",
 *                      },
 *                  ),
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Delete(
 *      path="/stations/{stationId}",
 *      summary="Delete station",
 *      description="Delete a station",
 *      operationId="deleteStation",
 *      tags={"Station"},
 *
 *      @OA\Parameter(
 *          description="ID of station",
 *          in="path",
 *          name="stationId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Station deleted"),
 *          )
 *      ),
 *
 *  )
 */
