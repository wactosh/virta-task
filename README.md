#### Installation

After cloning and successfully building docker containers and before using the app, run the following commands:

1. `docker-compose exec virta-task composer install`

2. `docker-compose exec virta-task php artisan migrate`

Swagger UI documentation should also be available at `<base_url>/api/documentation`

#### Testing
Make sure to have a database created named `virta_task_test` or according to the variable `TESTING_DB_DATABASE` set in the`.env` file.
