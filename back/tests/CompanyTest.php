<?php

use App\Services\CompanyService;
use App\Services\StationService;
use Laravel\Lumen\Testing\DatabaseMigrations;

class CompanyTest extends TestCase
{
    use DatabaseMigrations;

    protected $companyService;
    protected $stationService;

    public function setUp(): void
    {
        parent::setUp();

        $this->companyService = $this->app->make(CompanyService::class);
        $this->stationService = $this->app->make(StationService::class);
    }

    public function test_should_create_a_company_without_a_parent()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $this->post('/companies', $companyData)
            ->seeJson(['success' => true])
            ->seeStatusCode(201)
            ->seeJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'parent_company_id',
                    'created_at',
                    'updated_at'
                ]
            ])
            ->assertCount(1, $this->companyService->getAll());
    }

    public function test_should_create_a_company_with_a_parent()
    {
        $parentCompanyData = [
            'name' => 'Test parent company',
            'parent_company_id' => null,
        ];

        $parentCompany = $this->companyService->createOne($parentCompanyData);

        $childCompanyData = [
            'name' => 'Test child company',
            'parent_company_id' => $parentCompany->id,
        ];

        $this->post('/companies', $childCompanyData)
            ->seeJson(['success' => true])
            ->seeStatusCode(201)
            ->seeJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'parent_company_id',
                    'created_at',
                    'updated_at'
                ]
            ])
            ->assertCount(2, $this->companyService->getAll());
    }

    public function test_should_return_all_companies_with_a_parent()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $this->companyService->createOne($companyData);

        $this->get('/companies')
            ->seeJson(['success' => true])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'success',
                'message',
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'parent_company_id',
                        'companies',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }

    public function test_should_return_a_company()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $this->get('/companies/' . $company->id)
            ->seeJson(['success' => true])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'parent_company_id',
                    'companies',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    public function test_should_update_a_company()
    {
        $parentCompanyData = [
            'name' => 'Test parent company',
            'parent_company_id' => null,
        ];

        $parentCompany = $this->companyService->createOne($parentCompanyData);

        $childCompanyData = [
            'name' => 'Test child company',
            'parent_company_id' => null,
        ];

        $childCompany = $this->companyService->createOne($childCompanyData);

        $updatedChildCompanyData = [
            'name' => 'Updated test company name',
            'parent_company_id' => $parentCompany->id
        ];

        $this->put('/companies/' . $childCompany->id, $updatedChildCompanyData)
            ->seeJson(['success' => true])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'parent_company_id',
                    'companies',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    public function test_should_delete_a_company()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $this->delete('/companies/' . $company->id)
            ->seeJson(['success' => true])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'success',
                'message',
            ]);
    }

    public function test_should_return_a_company_with_stations_including_companies_with_stations()
    {
        $parentCompanyData = [
            'name' => 'Test parent company',
            'parent_company_id' => null,
        ];

        $parentCompany = $this->companyService->createOne($parentCompanyData);

        $parentCompanyStationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $parentCompany->id,
        ];

        $this->stationService->createOne($parentCompanyStationData);

        $childCompanyData = [
            'name' => 'Test child company',
            'parent_company_id' => $parentCompany->id,
        ];

        $childCompany = $this->companyService->createOne($childCompanyData);

        $childCompanyStationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $childCompany->id,
        ];

        $this->stationService->createOne($childCompanyStationData);

        $this->get('/companies/' . $parentCompany->id . '/stations')
            ->seeJson(['success' => true])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'parent_company_id',
                    'created_at',
                    'updated_at',
                    'stations' => [
                        '*' => [
                            'id',
                            'name',
                            'latitude',
                            'longitude',
                            'company_id',
                            'created_at',
                            'updated_at'
                        ]
                    ],
                    'companies' => [
                        '*' => [
                            'id',
                            'name',
                            'parent_company_id',
                            'created_at',
                            'updated_at',
                            'stations' => [
                                '*' => [
                                    'id',
                                    'name',
                                    'latitude',
                                    'longitude',
                                    'company_id',
                                    'created_at',
                                    'updated_at'
                                ]
                            ],
                            'companies'
                        ]
                    ],
                ]
            ]);
    }
}
