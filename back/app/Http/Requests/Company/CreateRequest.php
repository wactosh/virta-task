<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'parent_company_id' => ['nullable', 'integer', 'exists:companies,id']
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [];
    }
}
