<?php

namespace App\Repositories\Station;

use App\Models\Station;
use App\Repositories\BaseRepository;

class StationRepository extends BaseRepository implements StationRepositoryInterface
{
    /**
     * StationRepository constructor.
     *
     * @param Station $model
     */
    public function __construct(Station $model)
    {
        parent::__construct($model);
    }
}
