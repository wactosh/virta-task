<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface EloquentRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * @param int $modelId
     *
     * @return Model
     * @throws ModelNotFoundException
     */
    public function find(int $modelId): ?Model;

    /**
     * @param int   $modelId
     * @param array $attributes
     *
     * @return bool
     */
    public function update(int $modelId, array $attributes): bool;

    /**
     * @param int $modelId
     *
     * @return bool
     */
    public function delete(int $modelId): bool;
}
