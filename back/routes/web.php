<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'companies'], function () use ($router) {
    $router->get('/', 'CompanyController@index');
    $router->post('/', 'CompanyController@store');
    $router->get('/{companyId}', 'CompanyController@show');
    $router->put('/{companyId}', 'CompanyController@update');
    $router->delete('/{companyId}', 'CompanyController@destroy');
    $router->get('/{companyId}/stations', 'CompanyController@stations');
});

$router->group(['prefix' => 'stations'], function () use ($router) {
    $router->get('/', 'StationController@index');
    $router->post('/', 'StationController@store');
    $router->get('/{stationId}', 'StationController@show');
    $router->put('/{stationId}', 'StationController@update');
    $router->delete('/{stationId}', 'StationController@destroy');
});
