<?php

/**
 * @OA\Post(
 *      path="/companies/",
 *      summary="Create company",
 *      description="Create a company",
 *      operationId="createCompany",
 *      tags={"Company"},
 *
 *      @OA\RequestBody(
 *          required=true,
 *          description="Create company request",
 *          @OA\JsonContent(
 *              required={"name"},
 *              @OA\Property(property="name", type="string", example="company name"),
 *              @OA\Property(property="parent_company_id", type="number", example=null),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Company created"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<company>",
 *              )
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Invalid request response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=false),
 *              @OA\Property(property="message", type="string", example="The given data was invalid."),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="name",
 *                      type="string",
 *                      example={
 *                          "The name field is required.",
 *                      },
 *                  ),
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Get(
 *      path="/companies/",
 *      summary="Get companies",
 *      description="Get all companies including sub companies",
 *      operationId="getCompanies",
 *      tags={"Company"},
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Companies list"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<companies>",
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Get(
 *      path="/companies/{companyId}",
 *      summary="Get company",
 *      description="Get a company including sub companies",
 *      operationId="getCompany",
 *      tags={"Company"},
 *
 *      @OA\Parameter(
 *          description="ID of company",
 *          in="path",
 *          name="companyId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Company detail"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<company>",
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Get(
 *      path="/companies/{companyId}/stations",
 *      summary="Get company with stations",
 *      description="Get a company including sub companies with their sub stations",
 *      operationId="getCompanyStation",
 *      tags={"Company"},
 *
 *      @OA\Parameter(
 *          description="ID of company",
 *          in="path",
 *          name="companyId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Company children and stations"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<company>",
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Put(
 *      path="/companies/{companyId}",
 *      summary="Update company",
 *      description="Update a company",
 *      operationId="updateCompany",
 *      tags={"Company"},
 *
 *      @OA\Parameter(
 *          description="ID of company",
 *          in="path",
 *          name="companyId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\RequestBody(
 *          description="Create company request",
 *          @OA\JsonContent(
 *              @OA\Property(property="name", type="string", example="child company name"),
 *              @OA\Property(property="parent_company_id", type="number", example=1),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Company updated"),
 *              @OA\Property(
 *                  property="data",
 *                  type="object",
 *                  example="<company>",
 *              )
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Invalid request response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=false),
 *              @OA\Property(property="message", type="string", example="The given data was invalid."),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="parent_company_id",
 *                      type="string",
 *                      example={
 *                          "The selected parent company id is invalid.",
 *                      },
 *                  ),
 *              )
 *          )
 *      ),
 *  )
 */

/**
 * @OA\Delete(
 *      path="/companies/{companyId}",
 *      summary="Delete company",
 *      description="Delete a company",
 *      operationId="deleteCompany",
 *      tags={"Company"},
 *
 *      @OA\Parameter(
 *          description="ID of company",
 *          in="path",
 *          name="companyId",
 *          required=true,
 *          example="1",
 *          @OA\Schema(
 *              type="integer",
 *              format="int64"
 *          )
 *      ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Successful response",
 *          @OA\JsonContent(
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Company deleted"),
 *          )
 *      ),
 *
 *  )
 */
