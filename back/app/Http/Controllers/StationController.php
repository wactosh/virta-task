<?php

namespace App\Http\Controllers;

use App\Http\Requests\Station\CreateRequest;
use App\Http\Requests\Station\UpdateRequest;
use App\Services\StationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * @var StationService
     */
    private $stationService;

    /**
     * StationController constructor.
     *
     * @param StationService $stationService
     */
    public function __construct(StationService $stationService)
    {
        $this->stationService = $stationService;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $longitude = $request->query('longitude');
        $latitude = $request->query('latitude');

        $result = $latitude == null || $longitude == null
            ? $this->stationService->getAll()
            : $this->stationService->getAllWithinRadius($longitude, $latitude);

        return success('Stations list', $result);
    }

    /**
     * @param CreateRequest $request
     *
     * @return JsonResponse
     */
    public function store(CreateRequest $request)
    {
        $inputs = $request->only(['name', 'latitude', 'longitude', 'company_id']);

        $result = $this->stationService->createOne($inputs);

        if ($result) {
            return success('Station created', $result, 201);
        }

        return failed('An error occurred while creating station', 500);
    }

    /**
     * @param int $stationId
     *
     * @return JsonResponse
     */
    public function show(int $stationId)
    {
        return success('Station detail', $this->stationService->findOne($stationId));
    }

    /**
     * @param UpdateRequest $request
     * @param int           $stationId
     *
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, int $stationId)
    {
        $inputs = $request->only(['name', 'latitude', 'longitude', 'company_id']);

        $result = $this->stationService->updateOne($stationId, $inputs);

        if ($result) {
            $updatedStation = $this->stationService->findOne($stationId);

            return success('Station updated', $updatedStation);
        }

        return failed('An error occurred while updating station', 500);

    }

    /**
     * @param int $stationId
     *
     * @return JsonResponse
     */
    public function destroy(int $stationId)
    {
        $result = $this->stationService->deleteOne($stationId);

        if ($result) {
            return success('Station deleted');
        }

        return failed('An error occurred while deleting station', 500);
    }
}
