<?php

namespace App\Http\Requests\Station;

use App\Http\Requests\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
            'company_id' => ['required', 'integer', 'exists:companies,id']
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [];
    }
}
