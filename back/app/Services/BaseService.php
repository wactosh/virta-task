<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseService
{
    /**
     * @var mixed
     */
    public $repository;

    /**
     * @var int
     */
    protected $perPageLimit = 20;

    /**
     * BaseService constructor.
     *
     * @param $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->repository->all();
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function createOne(array $attributes): Model
    {
        return $this->repository->create($attributes);
    }

    /**
     * @param int $id
     *
     * @return Model|null
     */
    public function findOne(int $id): ?Model
    {
        return $this->repository->find($id);
    }

    /**
     * @param int   $id
     * @param array $attributes
     *
     * @return bool
     */
    public function updateOne(int $id, array $attributes): bool
    {
        return $this->repository->update($id, $attributes);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteOne(int $id): bool
    {
        return $this->repository->delete($id);
    }
}
