<?php

namespace App\Services;

use App\Repositories\Station\StationRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class StationService extends BaseService
{
    /**
     * StationService constructor.
     *
     * @param StationRepositoryInterface $stationRepository
     */
    public function __construct(StationRepositoryInterface $stationRepository)
    {
        parent::__construct($stationRepository);
    }

    /**
     * @param float $longitude
     * @param float $latitude
     *
     * @return Collection
     */
    public function getAllWithinRadius(float $longitude, float $latitude): Collection
    {
        return $this->repository->model
            ->whereRaw('
                ST_Distance_Sphere(
                    point(longitude, latitude),
                    point(?, ?)
                ) < ?
            ', [
                $longitude,
                $latitude,
                1000 // 1KM
            ])->get();
    }
}
