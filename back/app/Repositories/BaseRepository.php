<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
    public $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function all(): Collection
    {
        return $this->model->get();
    }

    /**
     * @inheritDoc
     */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @inheritDoc
     */
    public function find(int $modelId): ?Model
    {
        return $this->model->findOrFail($modelId);
    }

    /**
     * @inheritDoc
     */
    public function update(int $modelId, array $attributes): bool
    {
        return $this->model
            ->where('id', $modelId)
            ->update($attributes);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $modelId): bool
    {
        return $this->model
            ->findOrFail($modelId)
            ->delete();
    }
}
