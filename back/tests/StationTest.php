<?php

use App\Services\CompanyService;
use App\Services\StationService;
use Laravel\Lumen\Testing\DatabaseMigrations;

class StationTest extends TestCase
{
    use DatabaseMigrations;

    protected $companyService;
    protected $stationService;

    public function setUp(): void
    {
        parent::setUp();

        $this->companyService = $this->app->make(CompanyService::class);
        $this->stationService = $this->app->make(StationService::class);
    }

    public function test_should_create_a_station()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $stationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $company->id,
        ];

        $this->post('/stations', $stationData)->seeJson([
            'success' => true,
        ]);

        $this->assertCount(1, $this->stationService->getAll());

        $this->seeStatusCode(201);

        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                'id',
                'name',
                'latitude',
                'longitude',
                'company_id',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_should_return_all_stations()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $stationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $company->id,
        ];

        $this->stationService->createOne($stationData);

        $this->get('/stations')->seeJson([
            'success' => true,
        ]);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'latitude',
                    'longitude',
                    'company_id',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    public function test_should_return_all_stations_within_radius()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $stationData = [
            'name' => 'Test station',
            'latitude' => 35.739181,
            'longitude' => 51.43397,
            'company_id' => $company->id,
        ];

        $this->stationService->createOne($stationData);

        $this->get('/stations?longitude=51.424312&latitude=35.740961')->seeJson([
            'success' => true,
        ]);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'latitude',
                    'longitude',
                    'company_id',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    public function test_should_return_a_station()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $stationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $company->id,
        ];

        $station = $this->stationService->createOne($stationData);

        $this->get('/stations/' . $station->id)->seeJson([
            'success' => true,
        ]);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                'id',
                'name',
                'latitude',
                'longitude',
                'company_id',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_should_update_a_station()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $stationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $company->id,
        ];

        $station = $this->stationService->createOne($stationData);

        $updatedStationData = [
            'name' => 'Updated test station name',
            'latitude' => -2.1234567,
            'longitude' => 2.1234567,
            'company_id' => $company->id,
        ];

        $this->put('/stations/' . $station->id, $updatedStationData)->seeJson([
            'success' => true,
        ]);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                'id',
                'name',
                'latitude',
                'longitude',
                'company_id',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_should_delete_a_station()
    {
        $companyData = [
            'name' => 'Test company',
            'parent_company_id' => null,
        ];

        $company = $this->companyService->createOne($companyData);

        $stationData = [
            'name' => 'Test station',
            'latitude' => -1.1234567,
            'longitude' => 1.1234567,
            'company_id' => $company->id,
        ];

        $station = $this->stationService->createOne($stationData);

        $this->delete('/stations/' . $station->id)->seeJson([
            'success' => true,
        ]);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'success',
            'message',
        ]);
    }
}
