<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    /**
     * @inheritDoc
     */
    protected $fillable = ['name', 'parent_company_id'];

    /**
     * @param $value
     */
    public function setParentCompanyIdAttribute($value)
    {
        $this->attributes['parent_company_id'] = trim($value) == '' ? null : $value;
    }

    /**
     * @return HasMany
     */
    public function companies()
    {
        return $this->hasMany(self::class, 'parent_company_id')->with(['companies', 'stations']);
    }

    /**
     * @return HasMany
     */
    public function stations()
    {
        return $this->hasMany(Station::class);
    }
}
