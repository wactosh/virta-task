<?php

namespace App\Services;

use App\Models\Company;
use App\Repositories\Company\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CompanyService extends BaseService
{
    /**
     * CompanyService constructor.
     *
     * @param CompanyRepositoryInterface $companyRepository
     */
    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        parent::__construct($companyRepository);
    }

    /**
     * @return Collection
     */
    public function getAllWithChildCompanies(): Collection
    {
        return $this->repository->model->with('companies')->get();
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function createOne(array $attributes): Model
    {
        if (isset($attributes['parent_company_id'])) {
            $attributes['parent_company_id'] = trim($attributes['parent_company_id']) == '' ? null : $attributes['parent_company_id'];
        }

        return parent::createOne($attributes);
    }

    /**
     * @param int $companyId
     *
     * @return Company|null
     */
    public function findWithChildCompanies(int $companyId): ?Company
    {
        return $this->repository->model->with('companies')->findOrFail($companyId);
    }

    /**
     * @param int   $id
     * @param array $attributes
     *
     * @return bool
     */
    public function updateOne(int $id, array $attributes): bool
    {
        if (isset($attributes['parent_company_id'])) {
            $attributes['parent_company_id'] = trim($attributes['parent_company_id']) == '' ? null : $attributes['parent_company_id'];
        }

        return parent::updateOne($id, $attributes);
    }

    /**
     * @param int $companyId
     *
     * @return mixed
     */
    public function findStationsWithChildCompanies(int $companyId)
    {
        return $this->repository->model->with(['companies', 'stations'])->find($companyId);
    }
}
