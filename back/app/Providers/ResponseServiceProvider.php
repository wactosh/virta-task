<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Http\ResponseFactory;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the response services for the application.
     *
     * @param ResponseFactory $responseFactory
     */
    public function boot(ResponseFactory $responseFactory)
    {
        $responseFactory->macro('api',
            function (string $message,
                      $data = null,
                      int $status = 200,
                      array $headers = [],
                      int $options = 0) use ($responseFactory) {
                $success = $status < 400;

                $content = [
                    'success' => $success,
                    'message' => $message,
                ];

                if (!empty($data)) {
                    if ($success) {
                        $content['data'] = $data;
                    } else {
                        $content['errors'] = $data;
                    }
                }

                return $responseFactory->json($content, $status, $headers, $options);
            });
    }
}
