<?php

use Illuminate\Http\JsonResponse;

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     *
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('success')) {
    /**
     * Return successful response from the application.
     *
     * @param string $message
     * @param null   $data
     * @param int    $status
     *
     * @return JsonResponse
     */
    function success(string $message = '', $data = null, int $status = 200)
    {
        return response()->api($message, $data, $status);
    }
}

if (!function_exists('failed')) {
    /**
     * Return failed response from the application.
     *
     * @param string $message
     * @param int    $status
     *
     * @return JsonResponse
     */
    function failed(string $message = '', int $status = 500)
    {
        return response()->api($message, null, $status);
    }
}
